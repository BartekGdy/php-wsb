<?php
/**
 * Created by PhpStorm.
 * User: Alinka
 * Date: 2017-12-17
 * Time: 11:34
 */

class Person
{
    public $id;
    public $name;
    public $surname;

    /**
     * Person constructor.
     */
    public function __construct()
    {
        $this->setId(rand(1, 1000));
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getSurname()
    {
        return $this->surname;
    }

    /**
     * @param mixed $surname
     */
    public function setSurname($surname)
    {
        $this->surname = $surname;
    }

    public function __toString()
    {
        return get_class($this) . " {$this->id} $this->name $this->surname";
    }

}

class Form
{
    protected $hiddenFields = array('id');
    protected $object;

    /**
     * Form constructor.
     * @param $person
     */

    public function __construct($person)
    {
        $this->object = $person;
    }

    public function getObjectFields()
    {
        return get_object_vars($this->object);
    }

    public function generateForm($action = null)
    {

        $inputs = '';
        foreach ($this->getObjectFields() as $index => $value) {
            $inputs .= $this->generateInput($index, $value);
        }

        echo '<form action="' . $action . '" method="post">';
        echo $inputs;
        echo '<button type = "submit">Zapisz</button>';
        echo '</form>';
    }

    /**
     * @param $index
     * @param $value
     * @return string
     */
    private function generateInput($index, $value)
    {
        $inputs = '';
        $type = 'text';
        $label = "<label>$index</label>";
        if (in_array($index, $this->hiddenFields)) {
            $type = "hidden";
            $label = '';
        }
        $inputs .= "<div>$label <input type ='$type'name ='$index'value ='$value'></div>";
        return $inputs;
    }
}

class PostHandler
{
    public $post;

    public function __construct($post)
    {
        $this->post = $post;
    }

    public function isPostSet()
    {
        $isPostSet = false;
        if (isset($this->post) && !empty($this > $this->post)) {
            $isPostSet = true;
        }
        return $isPostSet;
    }
}


?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Person Form</title>
</head>
<body>
<?php
$person = new Person();
//$person->setName("John");
//$person->setSurname("Doe");
$form = new Form($person);
$form->generateForm();

$postHandler = new PostHandler($_POST);
if ($postHandler->isPostSet() == true) {
    var_dump($_POST);
}

?>
</body>
</html>
